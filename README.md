# BUGSPEED (proof of concept)

This repository contains various Makefile, scripts and data files
used during the experimental phase of the BUG SPEED project to
test its feasibility.

**NOTE: This is experimental, incomplete code.**

## Dependencies

`lintian`, `curl`, `coreutils`, `moreutils`, `make`.

## Organization

The analysis has been implemented as a series of Makefiles to
exploit the parallel nature of its substeps.

* `Makefile.1packages`: Fetches the list of all packages in the
   Debian historical archive from https://snapshot.debian.org.
* `Makefile.2versions`: Fetches the list of historical versions
   of the known packages.
* `Makefile.3reports`: Generates a lintian report for each package.
* `Makefile.4tags-instances`: For each known warning that lintian could
   produce, create an aggreated report by extracting the occurrences
   from the raw lintian reports.
* `Makefile.5tags-history`: Discovers when lintian gained the ability
   to detect a certain issue.
* `Makefile.analysis`: Creates statistical aggregations and graphs.

## To run the analysis

```
$ git clone https://salsa.debian.org/gioele/bugspeed.git -b poc
$ cd bugspeed
$ make -f Makefile.1packages
$ make -f Makefile.2versions -j3
$ make -f Makefile.3reports -j$(nproc)
$ #make -f Makefile.4tags-instances #TODO
$ make -f Makefile.5tags-history
$ make -f Makefile.analysis
```

The analysis will generate the file `dbus-policy-in-etc.data.raw`,
containing all the occurrences of the lintian issue `dbus-policy-in-etc`
found in the Debian historical archive.

The env variable `FOCUS` can be set to regular expression to limit
the analysis to a certain set of packages.

